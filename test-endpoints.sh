# Create user. Next executions will return an error as the user will already exist

RESPONSE=$(curl -s -XPOST http://localhost:5000/users/create -H 'Content-type: application/json' -d '{"username":"luke","password":"alderaan33"}')
echo $RESPONSE

# Get token
echo -e "\n\n"
TOKEN=$(curl -XPOST http://localhost:5000/auth/login  -H 'Content-type: application/json' -d '{"username":"luke","password":"alderaan33"}')

echo -e "\n\n"

echo "Retrieved JWT: $TOKEN"

# Retrieve object. First time from api, next time from DB. This can be checked in the logs of the app

curl -s -XGET http://localhost:5000/starships/22 -H "Authorization: Bearer $TOKEN" -H 'Content-type: application/json'

echo -e "\n\n"

# Optionally retrieve the object with id provided in the command line from the API
if [[ $# -ge 1 ]]
then
	curl -s -XGET http://localhost:5000/starships/$1 -H "Authorization: Bearer $TOKEN" -H 'Content-type: application/json'
	echo ""
fi

echo "Calling endpoint '/starships'. Saving to ALL_ITEMS"
curl -s -XGET http://localhost:5000/starships -H 'Content-type: application/json' > ALL_ITEMS


