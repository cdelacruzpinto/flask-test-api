from functools import lru_cache

import requests
import functools
import operator

class APIClient:
    def __init__(self, timeout=30, apiurl="https://swapi.dev/api"):
        self._timeout = timeout
        self._api_url = apiurl
        self.last_l_etag = ""
    def get_object(self, endpoint, id):
        r = requests.get(f"{self._api_url}/{endpoint}/{id}", timeout = self._timeout)
        return r.json()

    # Follows the links to retrieve all objects
    def get_objects(self, endpoint):
        objects = []
        try:
            current_url = f"{self._api_url}/{endpoint}"
            req = requests.get(current_url)
            self.last_l_etag = req.headers.get("etag")
            ss = req.json()
            while (ss['next'] != "null" and ss['next'] != None):
                objects.append(ss['results'])
                current_url = ss['next']
                ss = requests.get(current_url, timeout=self._timeout).json()
        except Exception as e:
             print (e)
        objects = functools.reduce(operator.iconcat, objects, [])
        return objects

    def get_etag(self, endpoint):
        url = f"{self._api_url}/{endpoint}"
        headers = requests.head(url, allow_redirects=True, timeout=self._timeout).headers
        return headers.get("etag")
    def last_list_etag(self):
        return self.last_l_etag
