import bcrypt
import os


def hash_pw(pw):
    pw = pw.encode('utf-8')
    salt = bcrypt.gensalt()
    hashed = bcrypt.hashpw(pw, bytes(salt))
    return hashed.decode('utf-8')


def verify_pw(hashed_pw, password):
    hashed_pw = hashed_pw.encode('utf-8')
    password = password.encode('utf-8')
    return bcrypt.checkpw(password, hashed_pw)


