from pymongo import MongoClient
from pymongo.errors import ConnectionFailure


class MongoDBLayer:
    def __init__(self, url):
        self.dburl = url
        self.client = MongoClient(self.dburl, serverSelectionTimeoutMS=4000, connectTimeoutMS=4000, socketTimeoutMS=1000)
        default = self.client.get_default_database()
        self.db = default
    def is_available(self):
        try:
            self.client.admin.command('ismaster')
            return True
        except ConnectionFailure:
            return False
    def get(self, collection, attribute, value):
        col = self.db[collection]
        cursor = col.find({attribute: value})
        if (cursor.count() == 0):
            return None
        try:
            obj = next(cursor)
            obj.pop('_id', None) # Not needed
            return obj
        except Exception as e:
            raise e
     
    def get_collection(self, collection):
        col = self.db[collection]
        cursor = col.find()
        items = []
        for obj in cursor:
            try:
                obj.pop('_id', None)  # Remove _id attribute to match original JSON schema
                items.append(obj)
            except:
                return None
        return items
    
    def add(self, collection, document):
        try:
            col = self.db[collection]
            result = col.insert_one(document)
            return str(result.inserted_id)
        except Exception as e:
            return None

