# Sample Flask / MongoDB Test API

* Demonstrates the use of PyMongo, requests and Flask
* /starships endpoint is protected by a decorator. Authorization: Bearer <valid JWT> is required
* To keep it simple, no JWKS is used for signing the token, but a simple string



## Running

Please make sure that you have either Docker working, or a local copy of MongoDB,
either with auth disabled, or dev/dev user account. Otherwise, *please tweak
the connection string at the top of app.py*

This was tested to run in Fedora 33 using Python 3.8 and pip3.

**./start-db.sh** -> Starts a docker container with MongoDB. Please make sure that there is an alias in /etc/hosts in the form: 127.0.0.1 mongodb if you are using the containerized version.

If not, please tweak the MongoDB connection string in app.py. 

**./start-api.sh** -> Makes a virtualenv with the required libraries and starts the application in development mode (i.e: no flask run)

* If it's not possible running the app in a virtualenv, a docker-compose.yaml file is provided as a convenience to allow running both mongo and the app as containers. Please run docker-compose up -d.

./test-api.sh -> Tests all the endpoints of the API using CURL when both services have been started

