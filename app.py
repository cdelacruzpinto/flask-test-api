import datetime
import json
import jwt
import logging
import re
import string
import flask

from functools import wraps
from http import HTTPStatus

from flask import jsonify, abort, make_response, Flask
from flask import request, redirect, url_for

from apiclient import apiclient
from datalayer import dblayer

from pw import hash_pw, verify_pw
from sys import exit

app = Flask(__name__)

OBJECTS_TYPE = "starships"
COLLECTION_NAME = OBJECTS_TYPE

DB_URL_DEV = "mongodb://dev:dev@mongodb:27017/starwars?authSource=admin"
CURRENT_ENV = "dev"

NON_ALPHA_REGEX = r'\W+'

settings = dict(
    dev=DB_URL_DEV
)

mongo = dblayer.MongoDBLayer(settings[CURRENT_ENV])
api = apiclient.APIClient()


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        auth_header = request.headers.get('Authorization')
        if auth_header is None:
            return redirect(url_for('unauthorized'))
        token = auth_header.split(" ")[1]  # "Bearer eyBlAh48sdj9wudhjjkwdkkdjdj"
        try:
            dec_token = jwt.decode(token, 'secret', algorithms=['HS256'])  # A wrong token triggers an exception
        except Exception as e:
            logging.error(e)
            return redirect(url_for('Unauthorized'))
        return f(*args, **kwargs)

    return decorated_function


STARSHIPS_COLLECTION = "starships"
STARSHIPS_ENDPOINT = "starships"
USERS_COLLECTION = "users"


@app.route("/users/create", methods=['POST'])
def create_user():
    user_info = flask.request.json
    try:
        username, password = user_info["username"], user_info["password"]
    except Exception as e:
        return "Wrong user format provided", HTTPStatus.BAD_REQUEST
    username = re.sub(NON_ALPHA_REGEX, '', username)  # strip non alpha symbols
    filtered_pw = ''.join(filter(lambda x: x in string.printable, password))  # filter non printable characters
    hashed_password = hash_pw(filtered_pw)
    try:
        ex = mongo.get(USERS_COLLECTION, "username", username)
        if (ex == None):
            mongo.add(USERS_COLLECTION, dict(username=username, password=str(hashed_password)))
            return json.dumps(dict(username=username, password=str(hashed_password)), default=str), HTTPStatus.OK
        else:
            logging.info("Try to create an already existing user")
            return "Error creating user. It may already exist. ", HTTPStatus.INTERNAL_SERVER_ERROR
    except:
        return "Error creating user", HTTPStatus.INTERNAL_SERVER_ERROR


@app.route("/auth/login", methods=['POST'])
def login():
    provided_user_info = flask.request.json
    try:
        username, password = provided_user_info["username"], provided_user_info["password"]
    except Exception as e:
        return "Wrong user / data structure provided", HTTPStatus.BAD_REQUEST
    username = re.sub(NON_ALPHA_REGEX, '', username)  # strip non alpha symbols
    filtered_pw = ''.join(filter(lambda x: x in string.printable, password))  # filter non printable characters
    try:
        db_user_info = mongo.get("users", "username", username)
        if db_user_info == None:
            return "Unauthorized", HTTPStatus.UNAUTHORIZED
        hashed_password = db_user_info["password"]
    except Exception as e:
        logging.error("Error retrieving username from DB! Possible configuration or coding issue")
        logging.error(e)
        return "Internal error", HTTPStatus.INTERNAL_SERVER_ERROR
    if verify_pw(hashed_password, filtered_pw):
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, seconds=600),
            'iat': datetime.datetime.utcnow(),
            'sub': username
        }
        return jwt.encode(payload, 'secret', algorithm='HS256'), HTTPStatus.OK
    return "", HTTPStatus.UNAUTHORIZED


@app.route("/unauthorized")
def unauthorized():
    return "Unauthorized", HTTPStatus.UNAUTHORIZED


@app.route("/starships")
def object_list():
    etag = api.get_etag(STARSHIPS_ENDPOINT)
    if (etag != api.last_list_etag()):
        try:
            objs = api.get_objects(STARSHIPS_ENDPOINT)
            for obj in objs:
                exists = mongo.get(STARSHIPS_COLLECTION, "url", obj["url"])
                if (exists == None):
                    mongo.add(STARSHIPS_COLLECTION, obj)
            return json.dumps(objs, default=str), HTTPStatus.OK
        except Exception as e:
            abort(make_response(jsonify(f"Remote api not available.{e}"), HTTPStatus.INTERNAL_SERVER_ERROR))
    try:
        objs = mongo.get_collection(STARSHIPS_COLLECTION)
    except:
        return "Error retrieving collection from DB", HTTPStatus.INTERNAL_SERVER_ERROR
    return json.dumps(objs, default=str), HTTPStatus.OK


@app.route(f"/starships/<int:object_id>")  # Defined at the top (starships)
@login_required
def fetch_object(object_id):
    url = f"http://swapi.dev/api/starships/{object_id}/"

    api_object = mongo.get(STARSHIPS_COLLECTION, "url", url)
    if (api_object == None):
        try:
            app.logger.warning(f"Fetching from DB object {object_id}")
            api_object = api.get_object(STARSHIPS_ENDPOINT, object_id)
        except Exception as e:
            app.logger.warning(f"Error contacting remote API {e}")
            return "Error contacting remote API", HTTPStatus.INTERNAL_SERVER_ERROR
        try:
            app.logger.warning(f"Caching object {object_id}")
            mongo.add(STARSHIPS_COLLECTION, api_object)
        except Exception as e:
            app.logger.error(f"Error persisting starship object locally {e}")
            return api_object, HTTPStatus.OK
    app.logger.warning(f"Retrieved cached object {object_id} from DB")
    return json.dumps(api_object, default=str)


if __name__ == "__main__":
    app.logger.setLevel(logging.DEBUG)
    logging.warning(f"===== Initializing starship microservice. =====")
    logging.warning(
        f"If no further messages are shown, please check that MongoDB is reachable in the following URL: {DB_URL_DEV}, or change it in the script=====")
    available = mongo.is_available()
    if not (available):
        logging.error(msg="DB not available")
        exit(1)
    logging.warning(f"Initializing app")
    app.run(host='0.0.0.0', debug=True)
