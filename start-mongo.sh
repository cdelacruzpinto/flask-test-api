#!/bin/bash

mkdir -p mongo
docker run -d --name db -e MONGO_INITDB_ROOT_USERNAME=dev -v data:/data/db -e MONGO_INITDB_ROOT_PASSWORD=dev -p 27017:27017 mongo &> /dev/null
if [[ $? -ne 0 ]]
then
	docker start db
fi
